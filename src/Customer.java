package com.emids.Insurance;

public class Customer {

	String name;
	String gender;
	int age;
	Health health;
	Habits habits;

	public Customer() {
	}

	public Customer(String name, String gender, int age) {
		this.name = name;
		this.gender = gender;
		this.age = age;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return this.name;
	}
	
	public String getGender() {
		return this.gender;
	}
	
	public int getAge() {
		return this.age;
	}
	
	public Health getHealth() {
		return this.health;
	}

	public void setHealth(Health health) {
		this.health = health;
	}

	public Habits getHabits() {
		return this.habits;
	}

	public void setHabits(Habits habits) {
		this.habits = habits;
	}

	public static void main(String[] args) {

		String customerName = "Norman Gomes";
		String gender = "Male";
		int  age =  34;
		int totalPremium = calculateInsPremium(customerName, gender, age);
		
		String initials="";
		if (gender.equalsIgnoreCase("male"))
			initials="Mr.";
		System.out.println("Health Insurance Premium for " + initials +   
				customerName + ": Rs. " + totalPremium);
		
	}

	public static int calculateInsPremium(String custName, String gender, int  age) {
		
		Customer customer = new  Customer(custName, gender, age);
		int basePremium = 5000;
		int totalPremium = basePremium;

		Health health = new  Health();
		health.setBloodpressure("No");
		health.setBloodsugar("No");
		health.setHypertension("No");
		health.setOverweigth("Yes");
		customer.setHealth(health);
		
		Habits habits = new Habits();
		habits.setSmoking("No");
		habits.setAlcohol("Yes");
		habits.setExercise("Yes");
		habits.setDrugs("No");
		customer.setHabits(habits);

		EmidUtils utils =  new  EmidUtils(customer);
		//Calculate premium based on age
		totalPremium = utils.calculateBasedOnAge(totalPremium);

		//Calculate premium based on gender
		totalPremium = utils.calculateBasedOnGender(totalPremium);
		
		//Calculate premium based on health
		totalPremium = utils.calculateBasedOnHealth(totalPremium);
		
		//Calculate premium based on habits
		totalPremium = utils.calculateBasedOnHabits(totalPremium);
		
		return totalPremium;
	}
	
}
