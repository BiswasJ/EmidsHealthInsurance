package com.emids.Insurance;

public class EmidUtils {

	Customer customer;
	public static Health health;
	public static Habits habits;
	
	public EmidUtils(Customer customer) {
		this.customer = customer;
	}
	
	public int calculateBasedOnAge(int totalPremium) {
		
		// Age-wise premium calculation
		int percentage=0;
		int age = customer.getAge();
		
		if ((age >= 18 && age < 25) || (age >= 25 && age < 30) || (age >= 30 && age < 35) || (age >= 35 && age < 40)) {
			percentage=10;
		} else if (age > 40) {
			percentage=20;
		}

		//Every 5-year calculation
		int totalYearsAfter18 = age - 18;
		int total5years =  18 % 5;
		for  (int i=1; i<= total5years; i++) {
			totalPremium = totalPremium + (totalPremium * percentage) / 100;
		}
		return totalPremium;
	}
	
	public int calculateBasedOnGender(int totalPremium) {
		
		// Gender-wise premium calculation
		String gender = customer.getGender();
		if (gender.equalsIgnoreCase("male")) {
			totalPremium = totalPremium + (totalPremium * 2) / 100;
		}
		return totalPremium;

	}
	
	
	public int calculateBasedOnHealth(int totalPremium) {
		// Pre-existing-condition-wise premium calculation
		health = customer.getHealth();
		if (health.getHypertension().equalsIgnoreCase("yes")) {
			totalPremium = totalPremium + (totalPremium * 1) / 100;
		}
		if (health.getBloodpressure().equalsIgnoreCase("yes")) {
			totalPremium = totalPremium + (totalPremium * 1) / 100;
		}
		if (health.getBloodsugar().equalsIgnoreCase("yes")) {
			totalPremium = totalPremium + (totalPremium * 1) / 100;
		}
		if (health.getOverweigth().equalsIgnoreCase("yes")) {
			totalPremium = totalPremium + (totalPremium * 1) / 100;
		}
		return totalPremium;
	}
	
	public int calculateBasedOnHabits(int totalPremium) {

		// Habits-wise premium calculation
		habits = customer.getHabits();
		if (habits.getSmoking().equalsIgnoreCase("yes")) {
			totalPremium = totalPremium + (totalPremium * 3) / 100;
		}
		if (habits.getAlcohol().equalsIgnoreCase("yes")) {
			totalPremium = totalPremium + (totalPremium * 3) / 100;
		}
		if (habits.getDrugs().equalsIgnoreCase("yes")) {
			totalPremium = totalPremium + (totalPremium * 3) / 100;
		}
		if (habits.getExercise().equalsIgnoreCase("yes")) {
			totalPremium = totalPremium - (totalPremium * 3) / 100;
		}
		return totalPremium;
		
	}

}
