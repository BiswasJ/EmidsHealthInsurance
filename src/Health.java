package com.emids.Insurance;

public class Health {

	String hypertension;
	String bloodpressure;
	String bloodsugar;
	String overweigth;
	
	public String getHypertension() {
		return hypertension;
	}
	public void setHypertension(String hypertension) {
		this.hypertension = hypertension;
	}
	public String getBloodpressure() {
		return bloodpressure;
	}
	public void setBloodpressure(String bloodpressure) {
		this.bloodpressure = bloodpressure;
	}
	public String getBloodsugar() {
		return bloodsugar;
	}
	public void setBloodsugar(String bloodsugar) {
		this.bloodsugar = bloodsugar;
	}
	public String getOverweigth() {
		return overweigth;
	}
	public void setOverweigth(String overweigth) {
		this.overweigth = overweigth;
	}
	
}
