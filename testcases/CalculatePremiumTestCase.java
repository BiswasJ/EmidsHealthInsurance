package testcases;

import org.junit.Test;
import static org.junit.Assert.assertEquals;


import com.emids.Insurance.*;

public class CalculatePremiumTestCase {

	@Test
	public void testPremiumCal() {
		Customer tester = new Customer();
		int premiumAmt = tester.calculateInsPremium("Norman Gomes", "Male", 26);
		assertEquals(6849, premiumAmt);
	}

}
//https://gitlab.com/BiswasJ/EmidsHealthInsurance.git